package ua.danit.application.model;

public class Hotel {
    private Long id;
    private String hotelName;
    private String hotelCity;
    private Long hotelStarRating;

    public Hotel(){
    }

   public Hotel(Long id, String hotelName, String hotelCity, Long hotelStarRating, boolean freeRooms ){
        this.id = id;
        this.hotelName = hotelName;
        this.hotelCity = hotelCity;
        this.hotelStarRating = hotelStarRating;
   }

    public Long getId() { return id; }

    public String getHotelName() {
        return hotelName;
    }

    public String getHotelCity() {
        return hotelCity;
    }

    public Long getHotelStarRating() {
        return hotelStarRating;
    }
}
